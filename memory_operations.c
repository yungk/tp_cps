#include "memory_operations.h"
#include <stdlib.h>

void *my_memcpy(void *dst, const void *src, size_t len) {
   char *c_src = (char *)src; 
   char *c_dst = (char *)dst; 

   for (int i=0; i<len; i++) {
       c_dst[i] = c_src[i];
   }
   return c_dst;
}

void *my_memmove(void *dst, const void *src, size_t len) {
    char *c_src = (char *)src; 
    char *c_dst = (char *)dst; 
    char cpy [len];
    
    for (int i=0; i<len; i++) {
        cpy[i] = c_src[i];
    }

    for (int i=0; i<len; i++) {
        c_dst[i] = cpy[i];
    }
    return c_dst;
}

int is_little_endian() {
    unsigned int i = 1;  
    char *c = (char*)&i;  
    if (*c == 1){
        return 1;
    } 
    else {
        return 0;
    }
}

int reverse_endianess(int value) {  
    int byte1;   
    int byte2;   
    int byte3;   
    int byte4; 
    int result;  
   
    byte1 = (value & 0x000000FF) >> 0;  
   
    byte2 = (value & 0x0000FF00) >> 8;  
  
    byte3 = (value & 0x00FF0000) >> 16;  
    
    byte4 = (value & 0xFF000000) >> 24;  

    byte1 <<= 24;  

    byte2 <<= 16;  
  
    byte3 <<= 8;  
 
    byte4 <<= 0;   

    result = (byte1 | byte2 | byte3 | byte4); 
  
    return result;  
}

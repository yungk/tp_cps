#include "vecteur.h"
#include <stdlib.h>

vecteur allouer_vecteur(int taille) {
    if (taille < 0) {
        vecteur v = {-1, NULL};
        return v;
    }
    else {
        vecteur v = { taille, malloc(taille*sizeof(double)) };
        return v;
    }
}

void liberer_vecteur(vecteur v) {
    free(v.donnees);
}

int est_vecteur_invalide(vecteur v) {
    if (v.taille < 0 || (v.donnees == NULL)) {
        return 1;
    }
    else {
        return 0;
    }
}

double *acces_vecteur(vecteur v, int i) {
    double *resultat = v.donnees+i;
    return resultat;
}

int taille_vecteur(vecteur v) {
    int resultat = v.taille;
    return resultat;
}

#include "vecteur_dynamique.h"
#include <stdlib.h>

vecteur allouer_vecteur(int taille) {
    vecteur v = malloc(sizeof(vecteur));
    if (taille < 0) {
        v->taille = -1;
        v->donnees = NULL;    
        return v;
    }
    else {
        v->taille = taille;
        v->donnees = malloc(taille*sizeof(double));
        return v;
    }
}

void liberer_vecteur(vecteur v) {
    free(v->donnees);
    free(v);
}

int est_vecteur_invalide(vecteur v) {
    if (v->taille < 0 || (v->donnees == NULL)) {
        return 1;
    }
    else {
        return 0;
    }
}

double *acces_vecteur(vecteur v, int i) {
    if (i < 0){
        return NULL;
    }
    else if (v->taille < i){
        v->taille = i + 1;
        v->donnees = realloc(v->donnees, v->taille*sizeof(double));
    }
    double *resultat = v->donnees+i;
    return resultat;
}

int taille_vecteur(vecteur v) {
    int resultat = v->taille;
    return resultat;
}
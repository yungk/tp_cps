#include "matrice_dynamique.h"
#include "vecteur_dynamique.h"
#include "vecteur_dynamique.c"
#include <stdlib.h>

matrice allouer_matrice(int l, int c) {
if (l < 0 || c < 0){
        matrice m = {-1, -1, NULL};
        return m;
    }
else {
        matrice m = { l, c, malloc(l*sizeof(double)) };
        for (int i = 0; i < l; i++) {
            m.donnees[i] =(void *)(allouer_vecteur(c)); //malloc(sizeof(vecteur));
            
        }
        return m;
    }
}

void liberer_matrice(matrice m) {
    for (int i=0; i<m.l;i++){
        liberer_vecteur((vecteur)m.donnees[i]);
    }
    free(m.donnees);
}

int est_matrice_invalide(matrice m) {
    if (m.l < 0 || m.c < 0 || m.donnees == NULL) {
        return 1;
    }
    else {
        return 0;
    }
}

double *acces_matrice(matrice m, int i, int j) {
    if ((i<0)||(j<0)){
        return NULL;
    }
    else if (m.l<i){
        m.l = i +1;
        m.donnees = realloc(m.donnees, m.l*sizeof(vecteur));
    }
    if (j>m.c){
        m.c = j+1;
        for (int u=0; u<i;u++){
        m.donnees[u] = acces_vecteur((vecteur)m.donnees[i], j);
        }
    }
    
    double *cancer = acces_vecteur((vecteur)m.donnees[i], j);
    return cancer;
}


int nb_lignes_matrice(matrice m) {

    return m.l;
}

int nb_colonnes_matrice(matrice m) {

    return m.c;
}


#include "matrice_lineaire.h"
#include <stdlib.h>

matrice allouer_matrice(int l, int c) {
    if (l < 0 || c < 0){
        matrice m = {-1, -1, NULL};
        return m;
    }
    else {
        matrice m = { l, c, malloc(l*c*sizeof(double)) };
        return m;
    }
}

void liberer_matrice(matrice m) {
    free(m.donnees);
}

int est_matrice_invalide(matrice m) {
    if (m.l < 0 || m.c < 0 || m.donnees == NULL) {
        return 1;
    }
    else {
        return 0;
    }
}

double *acces_matrice(matrice m, int i, int j) {
    double *resultat = &(m.donnees[i*m.c+j]);
    return resultat;
}

int nb_lignes_matrice(matrice m) {
    int resultat = m.l;
    return resultat;
}

int nb_colonnes_matrice(matrice m) {
    int resultat = m.c;
    return resultat;
}
